//
//  CustomUITextField.swift
//  Homepwner
//
//  Created by Alex Judy on 7/7/17.
//  Copyright © 2017 Alex Judy. All rights reserved.
//

import UIKit

class CustomUITextField : UITextField {
    
    override func becomeFirstResponder() -> Bool {
        borderStyle = .bezel
        super.becomeFirstResponder()
        return true
    }
    
    override func resignFirstResponder() -> Bool {
        borderStyle = .roundedRect
        super.resignFirstResponder()
        return true
    }
}
