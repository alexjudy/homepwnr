//
//  ItemCell.swift
//  Homepwner
//
//  Created by Alex Judy on 7/1/17.
//  Copyright © 2017 Alex Judy. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var serialNumberLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!

    func updateTextColor() {
        if let text = valueLabel.text {
            let startIndex = text.index(text.startIndex, offsetBy: 1)
            if let value = Double(text.substring(from: startIndex)) {

                if value < 50 {
                    valueLabel.textColor = UIColor.green
                } else {
                    valueLabel.textColor = UIColor.red
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateTextColor()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLabel.adjustsFontForContentSizeCategory = true
        serialNumberLabel.adjustsFontForContentSizeCategory = true
        valueLabel.adjustsFontForContentSizeCategory = true
    }
}
